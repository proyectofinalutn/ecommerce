<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;
use Carbon\Carbon;
use Image;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AdminUserController extends Controller
{
    public function AllAdminRole()
    {

        $adminuser = Admin::where('type', 2)->latest()->get();
        return view('backend.role.admin_role_all', compact('adminuser'));
    }


    public function AddAdminRole()
    {
        return view('backend.role.admin_role_create');
    }



    public function StoreAdminRole(Request $request)
    {
        $image = $request->file('profile_photo_path');
        $name_gen = hexdec(uniqid()) . '.' . $image->getClientOriginalExtension();
        Image::make($image)->resize(225, 225)->save('upload/admin_images/' . $name_gen);
        $save_url = 'upload/admin_images/' . $name_gen;

        Admin::insert([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'phone' => $request->phone,
            'brand' => $request->brand,
            'category' => $request->category,
            'product' => $request->product,
            'slider' => $request->slider,
            'coupons' => $request->coupons,

            'shipping' => $request->shipping,
            'setting' => $request->setting,
            'returnorder' => $request->returnorder,

            'orders' => $request->orders,
            'stock' => $request->stock,
            'reports' => $request->reports,
            'alluser' => $request->alluser,
            'adminuserrole' => $request->adminuserrole,
            'type' => 2,
            'profile_photo_path' => $save_url,
            'created_at' => Carbon::now(),


        ]);
    
        $notification = array(
            'message' => 'Usuario administrador creado exitosamente',
            'alert-type' => 'success'
        );

        return redirect()->route('all.admin.user')->with($notification);
    }



    // public function EditAdminRole($id)
    // {

    //     $adminuser = Admin::where('id', $id)
    //         ->where('type', 2)
    //         ->firstOrFail();

    //     return view('backend.role.admin_role_edit', compact('adminuser'));
    // }

    public function EditAdminRole($id)
    {

        $adminuser = Admin::findOrFail($id);
        return view('backend.role.admin_role_edit', compact('adminuser'));
    } 


    // public function UpdateAdminRole(Request $request)
    // {

    //     $admin_id = $request->id;
    //     $old_img = $request->old_image;

    //     if ($request->file('profile_photo_path')) {

    //         unlink($old_img);
    //         $image = $request->file('profile_photo_path');
    //         $name_gen = hexdec(uniqid()) . '.' . $image->getClientOriginalExtension();
    //         Image::make($image)->resize(225, 225)->save('upload/admin_images/' . $name_gen);
    //         $save_url = 'upload/admin_images/' . $name_gen;

    //         Admin::findOrFail($admin_id)->update([
    //             'name' => $request->name,
    //             'email' => $request->email,

    //             'phone' => $request->phone,
    //             'brand' => $request->brand,
    //             'category' => $request->category,
    //             'product' => $request->product,
    //             'slider' => $request->slider,
    //             'coupons' => $request->coupons,

    //             'shipping' => $request->shipping,
    //             'setting' => $request->setting,
    //             'returnorder' => $request->returnorder,

    //             'orders' => $request->orders,
    //             'stock' => $request->stock,
    //             'reports' => $request->reports,
    //             'alluser' => $request->alluser,
    //             'adminuserrole' => $request->adminuserrole,
    //             'type' => 2,
    //             'profile_photo_path' => $save_url,
    //             'created_at' => Carbon::now(),

    //         ]);
    //     //    dd($admin_id);
    //         $notification = array(
    //             'message' => 'Usuario administrador actualizado exitosamente',
    //             'alert-type' => 'info'
    //         );

    //         return redirect()->route('all.admin.user')->with($notification);
    //     } else {

    //     //    dd($admin_id);
    //         Admin::findOrFail($admin_id)->update([
    //             'name' => $request->name,
    //             'email' => $request->email,

    //             'phone' => $request->phone,
    //             'brand' => $request->brand,
    //             'category' => $request->category,
    //             'product' => $request->product,
    //             'slider' => $request->slider,
    //             'coupons' => $request->coupons,

    //             'shipping' => $request->shipping,
    //             'setting' => $request->setting,
    //             'returnorder' => $request->returnorder,

    //             'orders' => $request->orders,
    //             'stock' => $request->stock,
    //             'reports' => $request->reports,
    //             'alluser' => $request->alluser,
    //             'adminuserrole' => $request->adminuserrole,
    //             'type' => 2,

    //             'created_at' => Carbon::now(),

    //         ]);

    //         $notification = array(
    //             'message' => 'Usuario administrador actualizado exitosamente',
    //             'alert-type' => 'info'
    //         );

    //         return redirect()->route('all.admin.user')->with($notification);
    //     } 

    // }


    public function UpdateAdminRole(Request $request)
    {

        $admin_id = $request->id;
        $old_img = $request->old_image;

        if ($request->file('profile_photo_path')) {

            unlink($old_img);
            $image = $request->file('profile_photo_path');
            $name_gen = hexdec(uniqid()) . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(225, 225)->save('upload/admin_images/' . $name_gen);
            $save_url = 'upload/admin_images/' . $name_gen;

            Admin::findOrFail($admin_id)->update([
                'name' => $request->name,
                'email' => $request->email,

                'phone' => $request->phone,
                'brand' => $request->brand,
                'category' => $request->category,
                'product' => $request->product,
                'slider' => $request->slider,
                'coupons' => $request->coupons,

                'shipping' => $request->shipping,
                'setting' => $request->setting,
                'returnorder' => $request->returnorder,

                'orders' => $request->orders,
                'stock' => $request->stock,
                'reports' => $request->reports,
                'alluser' => $request->alluser,
                'adminuserrole' => $request->adminuserrole,
                'type' => 2,
                'profile_photo_path' => $save_url,
                'created_at' => Carbon::now(),

            ]);
        //    dd($admin_id);
            $notification = array(
                'message' => 'Usuario administrador actualizado exitosamente',
                'alert-type' => 'info'
            );

            return redirect()->route('all.admin.user')->with($notification);
        } else {

        //    dd($admin_id);
            Admin::findOrFail($admin_id)->update([
                'name' => $request->name,
                'email' => $request->email,

                'phone' => $request->phone,
                'brand' => $request->brand,
                'category' => $request->category,
                'product' => $request->product,
                'slider' => $request->slider,
                'coupons' => $request->coupons,

                'shipping' => $request->shipping,
                'setting' => $request->setting,
                'returnorder' => $request->returnorder,

                'orders' => $request->orders,
                'stock' => $request->stock,
                'reports' => $request->reports,
                'alluser' => $request->alluser,
                'adminuserrole' => $request->adminuserrole,
                'type' => 2,

                'created_at' => Carbon::now(),

            ]);

            $notification = array(
                'message' => 'Usuario administrador actualizado exitosamente',
                'alert-type' => 'info'
            );

            return redirect()->route('all.admin.user')->with($notification);
        } 

    }

    // public function UpdateAdminRole(Request $request)
    // {
    //     $admin_id = $request->id;
    //     $old_img = $request->old_image;

    //     if ($request->file('profile_photo_path')) {
    //         unlink($old_img);
    //         $image = $request->file('profile_photo_path');
    //         $name_gen = hexdec(uniqid()) . '.' . $image->getClientOriginalExtension();
    //         Image::make($image)->resize(225, 225)->save('upload/admin_images/' . $name_gen);
    //         $save_url = 'upload/admin_images/' . $name_gen;
    //     }

    //     $adminData = [
    //         'name' => $request->name,
    //         'email' => $request->email,
    //         'phone' => $request->phone,
    //         'brand' => $request->brand,
    //         'category' => $request->category,
    //         'product' => $request->product,
    //         'slider' => $request->slider,
    //         'coupons' => $request->coupons,
    //         'shipping' => $request->shipping,
    //         'setting' => $request->setting,
    //         'returnorder' => $request->returnorder,
    //         'orders' => $request->orders,
    //         'stock' => $request->stock,
    //         'reports' => $request->reports,
    //         'alluser' => $request->alluser,
    //         'adminuserrole' => $request->adminuserrole,
    //         'type' => 2, // Asegurando que el tipo sea igual a 2
    //         'created_at' => Carbon::now(),
    //     ];

    //     if (isset($save_url)) {
    //         $adminData['profile_photo_path'] = $save_url;
    //     }
    //     // dd($adminData);
    //     Admin::findOrFail($admin_id)->update($adminData);
    //     dd($adminData);
    //     $notification = array(
    //         'message' => 'Usuario administrador actualizado exitosamente',
    //         'alert-type' => 'info'
    //     );

    //     return redirect()->route('all.admin.user')->with($notification);
    // }




    public function DeleteAdminRole($id)
    {

        $adminimg = Admin::findOrFail($id);
        $img = $adminimg->profile_photo_path;

        if (file_exists($img)) {
            unlink($img);
        }

        Admin::findOrFail($id)->delete();

        $notification = array(
            'message' => 'Usuario administrador eliminado exitosamente',
            'alert-type' => 'info'
        );

        return redirect()->back()->with($notification);
    }

    public function AdminRolProfile()
    {
        $adminRolData = Admin::where('type', 2)->latest()->first();
        return view('admin.admin_rol_profile_view', compact('adminRolData'));
    }

    

    public function AdminRolProfileEdit(){
        $editRolData = Admin::where('type', 2)->latest()->first();
        return view('admin.admin_rol_profile_edit', compact('editRolData'));
    }


    public function AdminRolProfileStore(Request $request){
        $data = Admin::where('type', 2)->latest()->first();
        $data->name = $request->name;
        $data->email = $request->email;
        
        if($request->file('profile_photo_path')){
            $file = $request->file('profile_photo_path');
            @unlink(public_path('upload/admin_images/' .$data->profile_photo_path));
            $filename = date('YmdHi').$file->getClientOriginalName();
            $file->move(public_path('upload/admin_images/'), $filename);
            $data['profile_photo_path'] = $filename;
        }
        $data->save();

        $notifcation = array(
            'message' =>'Rol admin actualizado exitosamente',
            'alert-type' => 'success',
        );
        return redirect()->route('admin.rol.profile')->with($notifcation);
    }

    public function AdminRolChangePassword(){
        return view('admin.admin_rol_change_password');
    }

    public function AdminRolUpdateChangePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'oldpassword' => 'required',
            'password' => 'required|min:8|confirmed',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $admin = Admin::where('type', 2)->latest()->first();
        $hashedPassword = $admin->password;

        if (!Hash::check($request->oldpassword, $hashedPassword)) {
            $validator->errors()->add('oldpassword', 'La contraseña actual no es correcta.');
            return redirect()->back()->withErrors($validator)->withInput();
        }

        if ($request->password !== $request->password_confirmation) {
            $validator->errors()->add('password_confirmation', 'Las contraseñas no coinciden.');
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $admin->password = Hash::make($request->password);
        $admin->save();

        Auth::logout();

        return redirect()->route('admin.login')->with('success', '¡La contraseña se ha cambiado exitosamente!');
    }
}
