<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AdminProfileController extends Controller
{
    //

    public function AdminProfile(){
        $adminData = Admin::find(1);
        return view('admin.admin_profile_view', compact('adminData'));
    }

    public function AdminProfileEdit(){
        $editData = Admin::find(1);
        return view('admin.admin_profile_edit', compact('editData'));
    }
    

    public function AdminProfileStore(Request $request){
        $data = Admin::find(1);
        $data->name = $request->name;
        $data->email = $request->email;
        
        if($request->file('profile_photo_path')){
            $file = $request->file('profile_photo_path');
            @unlink(public_path('upload/admin_images/' .$data->profile_photo_path));
            $filename = date('YmdHi').$file->getClientOriginalName();
            $file->move(public_path('upload/admin_images'), $filename);
            $data['profile_photo_path'] = $filename;
        }
        $data->save();

        $notifcation = array(
            'message' =>'Admin Profile Updated Success',
            'alert-type' => 'success',
        );
        return redirect()->route('admin.profile')->with($notifcation);
    }

    public function AdminChangePassword(){
        return view('admin.admin_change_password');
    }

    // public function AdminUpdateChangePassword(Request $request){
    //     $validateData = $request->validate([
    //         'oldpassword' => 'required',
    //         'password' =>'required|confirmed',
    //     ]);

    //     $hashedPassword = Admin::find(1)->password;
    //     if (Hash::check($request->oldpassword,$hashedPassword)) {
    //         $admin = Admin::find(1);
    //         $admin->password = Hash::make($request->password);
    //         $admin->save();
    //         Auth::logout();
    //         return redirect()->route('admin.logout');

    //     }else{
    //         return redirect()->back();
    //     }
    // }

    public function AdminUpdateChangePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'oldpassword' => 'required',
            'password' => 'required|min:8|confirmed',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $admin = Admin::find(1);
        $hashedPassword = $admin->password;

        if (!Hash::check($request->oldpassword, $hashedPassword)) {
            $validator->errors()->add('oldpassword', 'La contraseña actual no es correcta.');
            return redirect()->back()->withErrors($validator)->withInput();
        }

        if ($request->password !== $request->password_confirmation) {
            $validator->errors()->add('password_confirmation', 'Las contraseñas no coinciden.');
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $admin->password = Hash::make($request->password);
        $admin->save();

        Auth::logout();

        return redirect()->route('admin.login')->with('success', '¡La contraseña se ha cambiado exitosamente!');
    }
    
}  