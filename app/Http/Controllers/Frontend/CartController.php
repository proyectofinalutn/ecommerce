<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Auth;
use App\Models\Wishlist;
use Carbon\Carbon;

use App\Models\Coupon;
use Illuminate\Support\Facades\Session;


use App\Models\ShipDivision;

class CartController extends Controller
{

    // public function AddToCart(Request $request, $id) {
    //     if (Session::has('coupon')) {
    //         Session::forget('coupon');
    //     }
    
    //     $product = Product::findOrFail($id);
    
    //     // Calculate the new price with the discount applied
    //     if ($product->discount_price != NULL) {
    //         $porDesc = $product->discount_price / 100;
    //         $desc = $product->selling_price * $porDesc;
    //         $newPrice = $product->selling_price - $desc;
    //     } else {
    //         // If there is no discount, use the selling price as the new price
    //         $newPrice = $product->selling_price;
    //     }
    
    //     Cart::add([
    //         'id' => $id,
    //         'name' => $request->product_name,
    //         'qty' => $request->quantity,
    //         'price' => $newPrice, // Use the calculated new price here
    //         'weight' => 1,
    //         'options' => [
    //             'image' => $product->product_thambnail,
    //             'color' => $request->color,
    //             'size' => $request->size,
    //         ],
    //     ]);
    
    //     return response()->json(['success' => 'Agregado exitosamente al carrito']);

    // }


    public function AddToCart(Request $request, $id) {
        if (Session::has('coupon')) {
            Session::forget('coupon');
        }

        $request->validate([
            'color' => 'required',
            'size' => 'required',
        ]);
    
        $product = Product::findOrFail($id);
    
        // Calcular el nuevo precio con el descuento aplicado (si aplica)
        $newPrice = $product->discount_price === NULL ? $product->selling_price : $product->selling_price - ($product->selling_price * $product->discount_price / 100);
    
        Cart::add([
            'id' => $id,
            'name' => $request->product_name,
            'qty' => $request->quantity,
            'price' => $newPrice, // Use the calculated new price here
            'weight' => 1,
            'options' => [
                'image' => $product->product_thambnail,
                'color' => $request->color,
                'size' => $request->size,
            ],
        ]);
    
        return response()->json(['success' => 'Agregado exitosamente al carrito']);
    }


    // public function AddToCart(Request $request, $id){
    //     if (Session::has('coupon')) {
    //         Session::forget('coupon');
    //     }
        
    //     $product = Product::findOrFail($id);

    //     if ($product->discount_price == NULL) {

    //         $porDesc = $product->discount_price / 100;
    //         $desc = $product->selling_price * $porDesc;
    //         $newPrice = $product->selling_price - $desc;

    //         Cart::add([
    //             'id' => $id,
    //             'name' => $request->product_name,
    //             'qty' => $request->quantity,
    //             'price' => $product->selling_price,
    //             'weight' => 1,
    //             'options' => [
    //                 'image' => $product->product_thambnail,
    //                 'color' => $request->color,
    //                 'size' => $request->size,
    //             ],
    //         ]);

    //         return response()->json(['success' => 'Agregado exitosamente al carrito']);
    //     } else {

    //         Cart::add([
    //             'id' => $id,
    //             'name' => $request->product_name,
    //             'qty' => $request->quantity,
    //             'price' => $product->discount_price,
    //             'weight' => 1,
    //             'options' => [
    //                 'image' => $product->product_thambnail,
    //                 'color' => $request->color,
    //                 'size' => $request->size,
    //             ],
    //         ]);
    //         return response()->json(['success' => 'Agregado exitosamente al carrito']);
    //     }
    // }

    public function AddMiniCart(){

        $carts = Cart::content();
        $cartQty = Cart::count();
        $cartTotal = Cart::total();

        return response()->json(array(
            'carts' => $carts,
            'cartQty' => $cartQty,
            'cartTotal' => $cartTotal,

        ));

    }

    public function RemoveMiniCart($rowId){
        Cart::remove($rowId);
        return response()->json(['success' => 'Producto removido del carrito']);
    }

    public function AddToWishlist(Request $request, $product_id){
        if (Auth::check()) {

            $exists = Wishlist::where('user_id', Auth::id())->where('product_id', $product_id)->first();

            if (!$exists) {
                Wishlist::insert([
                    'user_id' => Auth::id(),
                    'product_id' => $product_id,
                    'created_at' => Carbon::now(),
                ]);
                return response()->json(['success' => 'Añadido exitosamente a la lista de deseos']);
            } else {

                return response()->json(['error' => 'El producto ya está en su lista de deseos']);
            }
        } else {

            return response()->json(['error' => 'Debe iniciar sesión primeramente']);
        }
    }


    public function CouponApply(Request $request){
        $coupon = Coupon::where('coupon_name',$request->coupon_name)->where('coupon_validity','>=',Carbon::now()->format('Y-m-d'))->first();
        
        $total = (int)str_replace(',','',Cart::total());
        
        if ($coupon) {
            Session::put('coupon',[
                'coupon_name' => $coupon->coupon_name,
                'coupon_discount' => $coupon->coupon_discount,
                'discount_amount' => round($total * $coupon->coupon_discount/100), 
                'total_amount' => round($total - $total * $coupon->coupon_discount/100)  
            ]);

            return response()->json(array(

                'validity' => true,
                'success' => 'Coupon Applied Successfully'
            ));

        }else{
            return response()->json(['error' => 'Invalid Coupon']);
        }
    }
    
    public function CouponCalculation(){
        if (Session::has('coupon')) {
            return response()->json(array(
                'subtotal' => Cart::total(),
                'coupon_name' => session()->get('coupon')['coupon_name'],
                'coupon_discount' => session()->get('coupon')['coupon_discount'],
                'discount_amount' => session()->get('coupon')['discount_amount'],
                'total_amount' => session()->get('coupon')['total_amount'],
            ));
        }else{
            return response()->json(array(
                'total' => Cart::total(),
            ));

        }
    }

    public function CouponRemove(){
        Session::forget('coupon');
        return response()->json(['success' => 'Cupón removido exitosamente']);
    }


    public function CheckoutCreate(){
        if (Auth::check()) {
            if (Cart::total() > 0) {

        $carts = Cart::content();
        $cartQty = Cart::count();
        $cartTotal = Cart::total();

        $divisions = ShipDivision::orderBy('division_name','ASC')->get();
        return view('frontend.checkout.checkout_view',compact('carts','cartQty','cartTotal','divisions'));
                
            }else{

            $notification = array(
            'message' => 'Debe agregar productos al carrito',
            'alert-type' => 'error'
        );

        return redirect()->to('/')->with($notification);

            }

            
        }else{

            $notification = array(
            'message' => 'Debe iniciar sesión primeramente',
            'alert-type' => 'error'
        );

        return redirect()->route('login')->with($notification);

        }

    }

}
