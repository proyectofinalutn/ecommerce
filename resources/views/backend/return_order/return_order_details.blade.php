@extends('admin.admin_master')
@section('admin')

<!-- Main content -->
<section class="content">
        <div class="row">
            <div class="col-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Nombre </th>
                                        <th>Correo </th>
                                        <th>Teléfono </th>
                                        <th>Motivo solicitud </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($orders as $order)
                                    <tr>
                                        <td> {{ $order->name }} </td>
                                        <td> {{ $order->email }} </td>
                                        <td> {{ $order->phone }} </td>
                                        <td> {{ $order->return_reason }} </td>

                                    </tr>
                                    @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

@endsection