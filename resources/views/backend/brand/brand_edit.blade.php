@extends('admin.admin_master')
@section('admin')

<!-- Content Wrapper. Contains page content -->
<div class="container-full">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
        <div class="row">

            <!-- -------------------------- Edit Brand -------------------------------                 -->
            <div class="col-12">

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Editar Marcas</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            
                            <form method="post" action="{{ route('brand.update',$brand->id) }}" enctype="multipart/form-data">
                                @csrf
                                
                                <input type="hidden" name="id" value="{{ $brand->id }}">
                                <input type="hidden" name="old_image" value="{{ $brand->brand_image }}">
                                <!-- <div class="row"> -->
                                <!-- <div class="col-12"> -->

                                <div class="form-group">
                                    <h5>Nombre de Marca<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="brand_name" class="form-control" value="{{ $brand->brand_name }}">
                                        @error('brand_name')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                        <div class="help-block"></div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <h5>Imagen de la marca<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="file" name="brand_image" class="form-control">
                                        @error('brand_image')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                        <div class="help-block"></div>
                                    </div>
                                </div>

                                <div class="text-xs-right">
                                    <input type="submit" class="btn btn-rounded btn-success mb-5" value="Actualizar">
                                    <input href="{{ route('dashboard') }}" class="btn btn-rounded btn-success mb-5" value="Cancelar">
                                </div>
                                <!-- </div> -->
                                <!-- </div> -->
                        </div>
                        </form>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </div> <!-- /.col -->
    </section>
</div> <!-- /.content -->


@endsection