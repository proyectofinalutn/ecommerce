@extends('admin.admin_master')
@section('admin')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>

<div class="container-full">

    <section class="content">

        <!-- Basic Forms -->
        <div class="box">
            <div class="box-header with-border">
                <h4 class="box-title">Editar usuario administrador </h4>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col">
                        <form method="post" action="{{ route('admin.user.update') }}" enctype="multipart/form-data">
                            @csrf

                            <input type="hidden" name="id" value="{{ $adminuser->id }}">
                            <input type="hidden" name="old_image" value="{{ $adminuser->profile_photo_path }}">

                            <div class="row">
                                <div class="col-12">

                                    <div class="row">
                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <h5>Nombre del usuario administrador <span class="text-danger">*</span></h5>
                                                <div class="controls">
                                                    <input type="text" name="name" class="form-control" value="{{ $adminuser->name }}">
                                                </div>
                                            </div>

                                        </div> <!-- end cold md 6 -->



                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <h5>Correo usuario administrador <span class="text-danger">*</span></h5>
                                                <div class="controls">
                                                    <input type="email" name="email" class="form-control" value="{{ $adminuser->email }}">
                                                </div>
                                            </div>

                                        </div> <!-- end cold md 6 -->

                                    </div> <!-- end row 	 -->


                                    <div class="row">
                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <h5>Teléfono<span class="text-danger">*</span></h5>
                                                <div class="controls">
                                                    <input type="text" name="phone" class="form-control" value="{{ $adminuser->phone }}">
                                                </div>
                                            </div>

                                        </div> <!-- end cold md 6 -->

                                    </div> <!-- end row 	 -->


                                    <div class="row">

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <h5>Foto <span class="text-danger">*</span></h5>
                                                <div class="controls">
                                                    <input type="file" name="profile_photo_path" class="form-control" id="image">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                        <img id="showImage" src="{{ url('upload/no_image.jpg') }}" style="width: 100px; height: 100px;">
                                        </div>
                                    </div>

                                    <hr>

                                    <div class="row">

                                        <div class="col-md-4">
                                            <div class="form-group">

                                                <div class="controls">
                                                    <fieldset>
                                                        <input type="checkbox" id="checkbox_2" name="brand" value="1" {{ $adminuser->brand == 1 ? 'checked' : '' }}>
                                                        <label for="checkbox_2">Marcas</label>
                                                    </fieldset>
                                                    <fieldset>
                                                        <input type="checkbox" id="checkbox_3" name="category" value="1" {{ $adminuser->category == 1 ? 'checked' : '' }}>
                                                        <label for="checkbox_3">Categorías</label>
                                                    </fieldset>

                                                    <fieldset>
                                                        <input type="checkbox" id="checkbox_4" name="product" value="1" {{ $adminuser->product == 1 ? 'checked' : '' }}>
                                                        <label for="checkbox_4">Productos</label>
                                                    </fieldset>

                                                    <fieldset>
                                                        <input type="checkbox" id="checkbox_5" name="slider" value="1" {{ $adminuser->slider == 1 ? 'checked' : '' }}>
                                                        <label for="checkbox_5">Slider</label>
                                                    </fieldset>

                                                    <fieldset>
                                                        <input type="checkbox" id="checkbox_6" name="coupons" value="1" {{ $adminuser->coupons == 1 ? 'checked' : '' }}>
                                                        <label for="checkbox_6">Cupones</label>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>



                                        <div class="col-md-4">
                                            <div class="form-group">

                                                <div class="controls">
                                                    <fieldset>
                                                        <input type="checkbox" id="checkbox_7" name="shipping" value="1" {{ $adminuser->shipping == 1 ? 'checked' : '' }}>
                                                        <label for="checkbox_7">Zonas de envíos</label>
                                                    </fieldset>


                                                    <!-- <fieldset>
                                                        <input type="checkbox" id="checkbox_9" name="setting" value="1" {{ $adminuser->setting == 1 ? 'checked' : '' }}>
                                                        <label for="checkbox_9">Setting</label>
                                                    </fieldset> -->


                                                    <fieldset>
                                                        <input type="checkbox" id="checkbox_8" name="returnorder" value="1" {{ $adminuser->returnorder == 1 ? 'checked' : '' }}>
                                                        <label for="checkbox_8">Solicitudes devolución</label>
                                                    </fieldset>

                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-4">
                                            <div class="form-group">

                                                <div class="controls">
                                                    <fieldset>
                                                        <input type="checkbox" id="checkbox_9" name="orders" value="1" {{ $adminuser->orders == 1 ? 'checked' : '' }}>
                                                        <label for="checkbox_9">Pedidos</label>
                                                    </fieldset>
                                                    <fieldset>
                                                        <input type="checkbox" id="checkbox_10" name="stock" value="1" {{ $adminuser->stock == 1 ? 'checked' : '' }}>
                                                        <label for="checkbox_10">Existencias</label>
                                                    </fieldset>

                                                    <fieldset>
                                                        <input type="checkbox" id="checkbox_11" name="reports" value="1" {{ $adminuser->reports == 1 ? 'checked' : '' }}>
                                                        <label for="checkbox_11">Reportes</label>
                                                    </fieldset>

                                                    <!-- <fieldset>
                                                        <input type="checkbox" id="checkbox_15" name="alluser" value="1" {{ $adminuser->alluser == 1 ? 'checked' : '' }}>
                                                        <label for="checkbox_15">Alluser</label>
                                                    </fieldset> -->

                                                    <fieldset>
                                                        <input type="checkbox" id="checkbox_12" name="adminuserrole" value="1" {{ $adminuser->adminuserrole == 1 ? 'checked' : '' }}>
                                                        <label for="checkbox_12">Usuarios administradores</label>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="text-xs-right">
                                        <input type="submit" class="btn btn-rounded btn-primary mb-5" value="Actualizar">
                                    </div>
                        </form>

                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </section>


</div>


<script type="text/javascript">
    $(document).ready(function() {
        $('#image').change(function(e) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#showImage').attr('src', e.target.result);
            }
            reader.readAsDataURL(e.target.files['0']);
        });
    });
</script>


@endsection