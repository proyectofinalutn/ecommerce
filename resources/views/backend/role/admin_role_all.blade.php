@extends('admin.admin_master')
@section('admin')


<!-- Content Wrapper. Contains page content -->

<div class="container-full">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Usuarios administadores </h3>
                        <a href="{{ route('add.admin') }}" class="btn btn-danger" style="float: right;">Crear usuario Admin</a>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Imagen </th>
                                        <th>Nombre </th>
                                        <th>Correo </th>
                                        <th>Accesos </th>
                                        <th>Acciones</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($adminuser as $item)
                                    <tr>
                                        <td> <img src="{{ asset($item->profile_photo_path) }}" style="width: 50px; height: 50px;"> </td>
                                        <td> {{ $item->name }} </td>
                                        <td> {{ $item->email  }} </td>

                                        <td>
                                            @if($item->brand == 1)
                                            <span class="badge btn-primary">Marcas</span>
                                            @else
                                            @endif

                                            @if($item->category == 1)
                                            <span class="badge btn-secondary">Categorías</span>
                                            @else
                                            @endif


                                            @if($item->product == 1)
                                            <span class="badge btn-success">Productos</span>
                                            @else
                                            @endif


                                            @if($item->slider == 1)
                                            <span class="badge btn-danger">Slider</span>
                                            @else
                                            @endif


                                            @if($item->coupons == 1)
                                            <span class="badge btn-warning">Cupones</span>
                                            @else
                                            @endif


                                            @if($item->shipping == 1)
                                            <span class="badge btn-info">Zonas de envíos</span>
                                            @else
                                            @endif



                                            @if($item->returnorder == 1)
                                            <span class="badge btn-primary">Solicitudes devolución</span>
                                            @else
                                            @endif




                                            @if($item->orders == 1)
                                            <span class="badge btn-success">Pedidos</span>
                                            @else
                                            @endif

                                            @if($item->stock == 1)
                                            <span class="badge btn-danger">Existencias</span>
                                            @else
                                            @endif

                                            @if($item->reports == 1)
                                            <span class="badge btn-warning">Reportes</span>
                                            @else
                                            @endif

                                            @if($item->adminuserrole == 1)
                                            <span class="badge btn-dark">Usuarios administradores</span>
                                            @else
                                            @endif

                                        </td>
                                        <td width="25%">
                                            <a href="{{ route('edit.admin.user',$item->id) }}" class="btn btn-info" title="Editar"><i class="fa fa-pencil"></i> </a>

                                            <a href="{{ route('delete.admin.user',$item->id) }}" class="btn btn-danger" title="Eliminar" id="delete">
                                                <i class="fa fa-trash"></i></a>
                                        </td>

                                    </tr>
                                    @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            <!-- /.col -->

        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

</div>

@endsection