@extends('admin.admin_master')
@section('admin')


<!-- Content Wrapper. Contains page content -->
<div class="container-full">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-8">

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Lista de Slider <span class="badge badge-pill badge-success"> {{ count($sliders) }}</span></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Imagen Slider</th>
                                        <th>Título</th>
                                        <th>Descripción</th>
                                        <th>Estado</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($sliders as $item)
                                    <tr>
                                        <td><img src="{{ asset($item->slider_img) }}" style="width: 70px; height: 40px;"></td>
                                        <td>
                                            @if($item->title == NULL)
                                            <span class="badge badge-pill badge-danger"> Sin título </span>
                                            @else
                                            {{ $item->title }}
                                            @endif
                                        </td>
                                        <td>{{ $item->description }}</td>
                                        <td>
                                            @if($item->status == 1)
                                            <span class="badge badge-pill badge-success"> Activo </span>
                                            @else
                                            <span class="badge badge-pill badge-danger"> Inactivo </span>
                                            @endif
                                        </td>
                                        <td width="30%">
                                            <a href="{{ route('slider.edit',$item->id )}}" class="btn btn-warning" title="Editar"><i class="fa fa-pencil"></i></a>
                                            <a href="{{ route('slider.delete',$item->id )}}" class="btn btn-danger" id="delete" title="Eliminar"><i class="fa fa-trash"></i></a>

                                            @if($item->status == 1)
                                            <a href="{{ route('slider.inactive',$item->id) }}" class="btn btn-danger" title="Inactivar"><i class="fa fa-arrow-down"></i> </a>
                                            @else
                                            <a href="{{ route('slider.active',$item->id) }}" class="btn btn-success" title="Activar"><i class="fa fa-arrow-up"></i> </a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            <!-- /.col -->

            <!-- -------------------------- Add Slider -------------------------------                 -->
            <div class="col-4">

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Agregar Slider</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <form method="post" action="{{ route('slider.store') }}" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <h5>Título Slider<span class="text-danger"></span></h5>
                                    <div class="controls">
                                        <input type="text" name="title" class="form-control">
                                        @error('title')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                        <div class="help-block"></div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <h5>Descripción slider<span class="text-danger"></span></h5>
                                    <div class="controls">
                                        <input type="text" name="description" class="form-control">
                                        @error('description')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                        <div class="help-block"></div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <h5>Imagen slider<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="file" name="slider_img" class="form-control">
                                        @error('slider_img')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                        <div class="help-block"></div>
                                    </div>
                                </div>

                                <div class="text-xs-right">
                                    <input type="submit" class="btn btn-rounded btn-success mb-5" value="Agregar">
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </div>
        <!-- /.row -->
    </section>
</div>


@endsection