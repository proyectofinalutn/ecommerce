@extends('admin.admin_master')
@section('admin')

<!-- Content Wrapper. Contains page content -->

<div class="container-full">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Lista de existencias de productos <span class="badge badge-pill badge-danger"> {{ count($products) }} </span></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Imagen </th>
                                        <th>Nombre Producto</th>
                                        <th>Precio del producto</th>
                                        <th>Cantidad </th>
                                        <th>Descuento </th>
                                        <th>Estado </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($products as $item)
                                    <tr>
                                        <td> <img src="{{ asset($item->product_thambnail) }}" style="width: 60px; height: 50px;"> </td>
                                        <td>{{ $item->product_name }}</td>
                                        <td>{{ $item->selling_price }} ₡</td>
                                        <td>{{ $item->product_qty }} </td>
                                        <td>
                                            @if($item->discount_price == NULL)
                                            <span class="badge badge-pill badge-danger">Sin descuento</span>
                                            @else
                                            @php
                                            $porDesc = $item->discount_price / 100;
                                            $desc = $item->selling_price * $porDesc;
                                            $newPrice = $item->selling_price - $desc;
                                            @endphp
                                            <span class="badge badge-pill badge-primary">{{ $item->discount_price  }} %</span>

                                            @endif
                                        </td>

                                        <td>
                                            @if($item->status == 1)
                                            <span class="badge badge-pill badge-success"> Activo </span>
                                            @else
                                            <span class="badge badge-pill badge-danger"> Inactivo </span>
                                            @endif

                                        </td>

                                    </tr>
                                    @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

</div>

@endsection