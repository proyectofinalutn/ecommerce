@extends('admin.admin_master')
@section('admin')

@php

$products = DB::table('products')->get();
$brand = DB::table('brands')->get();


$orders = DB::table('orders')->get();

// Filtrar los datos para obtener ventas diarias
    $dailySalesData = [
        'labels' => $orders->pluck('order_date')->toArray(),
        'datasets' => [
            [
                'label' => 'Ventas Diarias',
                'data' => $orders->pluck('amount')->toArray(),
                'backgroundColor' => 'rgba(0, 138, 0, 0.9)',
                'borderColor' => 'rgba(0, 138, 0, 0.9)',
                'borderWidth' => 1,
            ]
        ],
    ];

    // Filtrar los datos para obtener ventas por estado (año específico)
    $statusSalesData = [
        'labels' => ['Pendiente', 'Cancelado', 'Confirmado', 'Entregado'],
        'datasets' => [
            [
                'label' => 'Cantidad',
                'data' => [
					$orders->where('status', 'Pendiente')->count(),
                    $orders->where('status', 'cancelado')->count(),
                    $orders->where('status', 'confirmado')->count(),
                    $orders->where('status', 'entregado')->count(),
                    
                ],
                'backgroundColor' => [
					'rgba(255, 163, 0, 1)',
                    'rgba(169, 0, 0, 1)',
                    'rgba(26, 55, 203, 1)',
                    'rgba(0, 163, 0, 1)',
                ],
                'borderColor' => [
					'rgba(255, 163, 0, 1)',
					'rgba(169, 0, 0, 1)',
                    'rgba(26, 55, 203, 1)',
                    'rgba(0, 163, 0, 1)',
                ],
                'borderWidth' => 1,
            ]
        ],
    ];

	// Filtrar los datos para obtener ventas por mes (año específico)
            $monthlySalesData = [
                'labels' => ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'], // Nombres de los meses
                'datasets' => [
                    [
                        'label' => 'Ventas por Mes',
                        'data' => [
                            $orders->where('order_month', 'January')->where('order_year', 2023)->sum('amount'),
                            $orders->where('order_month', 'February')->where('order_year', 2023)->sum('amount'),
                            $orders->where('order_month', 'March')->where('order_year', 2023)->sum('amount'),
                            $orders->where('order_month', 'April')->where('order_year', 2023)->sum('amount'),
                            $orders->where('order_month', 'May')->where('order_year', 2023)->sum('amount'),
                            $orders->where('order_month', 'June')->where('order_year', 2023)->sum('amount'),
                            $orders->where('order_month', 'July')->where('order_year', 2023)->sum('amount'),
                            $orders->where('order_month', 'August')->where('order_year', 2023)->sum('amount'),
                            $orders->where('order_month', 'September')->where('order_year', 2023)->sum('amount'),
                            $orders->where('order_month', 'October')->where('order_year', 2023)->sum('amount'),
                            $orders->where('order_month', 'November')->where('order_year', 2023)->sum('amount'),
                            $orders->where('order_month', 'December')->where('order_year', 2023)->sum('amount'),
                        ],
                        'backgroundColor' => 'rgba(54, 162, 235, 0.2)',
                        'borderColor' => 'rgba(54, 162, 235, 1)',
                        'borderWidth' => 1,
                    ]
                ],
            ];

			// Filtrar los datos para obtener ventas por año
            $yearlySalesData = [
                'labels' => $orders->pluck('order_year')->unique()->toArray(), // Obtener los años únicos
                'datasets' => [
                    [
                        'label' => 'Ventas por Año',
                        'data' => [
                            $orders->where('order_year', $orders->pluck('order_year')->unique()->toArray()[0])->sum('amount'),


                            
                        ],
                        'backgroundColor' => 'rgba(255, 159, 64, 0.2)',
                        'borderColor' => 'rgba(255, 159, 64, 1)',
                        'borderWidth' => 1,
                    ]
                ],
            ];


$orderItems = DB::table('order_items')->get();

// Filtrar los datos para obtener ventas por producto
			$productSalesData = [
                'labels' => [],
                'datasets' => [
                    [
                        'label' => 'Ventas por Producto',
                        'data' => [],
                    
						'backgroundColor' => [], // Array para los colores de fondo de las barras
                        'borderColor' => 'rgba(153, 102, 255, 1)',
                        'borderWidth' => 1,
                    ]
                ],
            ];

			// Paleta de colores predefinida
			$colors = [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)',
            
			];

            // Calcular el total de ventas por producto
            foreach ($products as $index => $product) {
                $totalSales = $orderItems->where('product_id', $product->id)->sum(function ($item) {
                    return $item->qty * $item->price;
                });

                $productSalesData['labels'][] = $product->product_name; // Agregar el nombre del producto como etiqueta en el gráfico
                $productSalesData['datasets'][0]['data'][] = $totalSales;
				$productSalesData['datasets'][0]['backgroundColor'][] = $colors[$index % count($colors)]; // Asignamos un color diferente a cada barra
            }


$categories = DB::table('categories')->get();

// Calcular la distribución de ventas por categoría
        $categorySalesData = [
            'labels' => [], // Array para las etiquetas de las categorías
            'datasets' => [
                [
                    'label' => 'Distribución de Ventas por Categoría',
                    'data' => [],
                    'backgroundColor' => [], // Array para los colores de las secciones del pastel
                ]
            ],
        ];

        // Paleta de colores predefinida (puedes modificarla según tus preferencias)
        $colors = [
            'rgba(255, 99, 132, 0.8)',
            'rgba(54, 162, 235, 0.8)',
            'rgba(75, 192, 192, 0.8)',
            'rgba(153, 102, 255, 0.8)',
            'rgba(255, 159, 64, 0.8)',
            // Añade más colores si necesitas
        ];

        // Calcular el total de ventas por categoría
        foreach ($categories as $index => $category) {
            $totalSales = 0;
            $productsInCategory = $products->where('category_id', $category->id);

            foreach ($productsInCategory as $product) {
                $totalSales += $orderItems->where('product_id', $product->id)->sum(function ($item) {
                    return $item->qty * $item->price;
                });
            }

            // Agregar el nombre de la categoría y los datos de ventas al gráfico de pastel
            $categorySalesData['labels'][] = $category->category_name; // Utilizamos la propiedad "name" para obtener el nombre de la categoría
            $categorySalesData['datasets'][0]['data'][] = $totalSales;
            $categorySalesData['datasets'][0]['backgroundColor'][] = $colors[$index % count($colors)];
        }

$brands = DB::table('brands')->get();		

		// Calcular el total de ventas por marca
        $brandSalesData = [
            'labels' => [], // Array para las etiquetas de las marcas
            'datasets' => [
                [
                    'label' => 'Ventas por Marca',
                    'data' => [],
                    'backgroundColor' => [], // Array para los colores de las barras
                ]
            ],
        ];

        // Paleta de colores predefinida (puedes modificarla según tus preferencias)
        $colors = [
            'rgba(255, 99, 132, 0.8)',
            'rgba(54, 162, 235, 0.8)',
            'rgba(75, 192, 192, 0.8)',
            'rgba(153, 102, 255, 0.8)',
            'rgba(255, 159, 64, 0.8)',
            // Añade más colores si necesitas
        ];

        foreach ($brands as $index => $brand) {
            $totalSales = 0;

            // Obtener los productos asociados a la marca
            $productsInBrand = $products->where('brand_id', $brand->id);

            foreach ($productsInBrand as $product) {
                // Obtener el total de ventas del producto
                $totalSales += $orderItems->where('product_id', $product->id)->sum(function ($item) {
                    return $item->qty * $item->price;
                });
            }

            // Agregar el nombre de la marca y el total de ventas al gráfico
            $brandSalesData['labels'][] = $brand->brand_name; // Utilizamos la propiedad "name" para obtener el nombre de la marca
            $brandSalesData['datasets'][0]['data'][] = $totalSales;
            $brandSalesData['datasets'][0]['backgroundColor'][] = $colors[$index % count($colors)];
        }

@endphp
<div class="container-full">
	<section class="content">
		<div class="row">

			<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
			<!-- Incluir chartjs-plugin-datalabels -->
			<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels"></script>

			<style>
				/* Personaliza el tamaño del contenedor del gráfico según tus necesidades */
				.chart-container {
					width: 80%;
					margin: auto;
				}
			</style>
			<div class="col-lg-6">
				<div class="box">
					<div class="box-body">		
						<h1>Ventas Diarias</h1>
						<div class="chart-container">
							<canvas id="dailySalesChart"></canvas>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="box">
					<div class="box-body">		
						<h1>Estado de los pedidos (año específico)</h1>
						<div class="chart-container">
							<canvas id="statusSalesChart"></canvas>
						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-6">
				<div class="box">
					<div class="box-body">
							<h1>Ventas por mes</h1>
							<div class="chart-container">
								<canvas id="monthlySalesChart"></canvas>
							</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="box">
					<div class="box-body">
							<h1>Ventas por Año</h1>
							<div class="chart-container">
								<canvas id="yearlySalesChart"></canvas>
							</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="box">
					<div class="box-body">
							<h1>Ventas por productos</h1>
							<div class="chart-container">
								<canvas id="productSalesChart"></canvas>
							</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="box">
					<div class="box-body">
						<h1>Gráfico de Distribución de Ventas por Categoría</h1>
						<div class="chart-container">
							<canvas id="categorySalesChart"></canvas>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="box">
					<div class="box-body">
						<h1>Ventas por Marcas</h1>
						<div class="chart-container">
							<canvas id="brandSalesChart"></canvas>
						</div>
					</div>
				</div>
			</div>

			
    <script>
        // Datos para el gráfico de ventas diarias
        var dailySalesData = {!! json_encode($dailySalesData) !!};

        // Datos para el gráfico de ventas por estado 
        var statusSalesData = {!! json_encode($statusSalesData) !!};
        
		// Datos para el gráfico de ventas por estado
        var monthlySalesData = {!! json_encode($monthlySalesData) !!};
		
		// Datos para el gráfico de ventas por estado
        var yearlySalesData = {!! json_encode($yearlySalesData) !!};

		// Datos para el gráfico de ventas por producto
        var productSalesData = {!! json_encode($productSalesData) !!};
		
		// Datos para el gráfico de ventas por categorias
        var categorySalesData = {!! json_encode($categorySalesData) !!}; 

		// Datos para el gráfico de ventas por marcas
        var brandSalesData = {!! json_encode($brandSalesData) !!}; 
		
        // Configuración del gráfico de ventas diarias
        var dailySalesCtx = document.getElementById('dailySalesChart').getContext('2d');
        var dailySalesChart = new Chart(dailySalesCtx, {
            type: 'line',
            data: dailySalesData,
            options: {
				responsive: true,
				
			title: {
            display: true,
            text: 'Ventas Diarias', // Título del gráfico
            fontSize: 18,
            fontColor: '#333', // Color del texto del título
            fontStyle: 'bold',
            },
			
            }
        });

        // Configuración del gráfico de ventas por estado
        var statusSalesCtx = document.getElementById('statusSalesChart').getContext('2d');
        var statusSalesChart = new Chart(statusSalesCtx, {
            type: 'doughnut',
            data: statusSalesData,
            options: {
                responsive: true,
				maintainAspectRatio: false,
				plugins: {
					legend: {
        			position: 'top',
			},
			title: {
        	display: true,
        	text: 'Estados de los pedidos'
			}
    }
            }
        });

		// Configuración del gráfico de ventas por mes
        var monthlySalesCtx = document.getElementById('monthlySalesChart').getContext('2d');
        var monthlySalesChart = new Chart(monthlySalesCtx, {
            type: 'bar',
            data: monthlySalesData, // Convierte la variable PHP a JSON
            options: {
                
            }
        });

		 // Configuración del gráfico de ventas por año
		var yearlySalesCtx = document.getElementById('yearlySalesChart').getContext('2d');
        var yearlySalesChart = new Chart(yearlySalesCtx, {
            type: 'bar',
            data: yearlySalesData, // Convierte la variable PHP a JSON
            options: {
                // Aquí puedes configurar más opciones del gráfico de ventas por año
                responsive: true,
                maintainAspectRatio: false,
                title: {
                    display: true,
                    text: 'Ventas por Año',
                    fontSize: 18,
                    fontColor: '#333',
                    fontStyle: 'bold',
                },
                scales: {
                    x: {
                        display: true,
                        title: {
                            display: true,
                            text: 'Año',
                            fontSize: 14,
                            fontColor: '#666',
                            fontStyle: 'normal',
                        },
                    },
                    y: {
                        display: true,
                        title: {
                            display: true,
                            text: 'Monto',
                            fontSize: 14,
                            fontColor: '#666',
                            fontStyle: 'normal',
                        },
                        ticks: {
                            beginAtZero: true,
                            stepSize: 100,
                        },
                    },
                },
                legend: {
                    display: true,
                    position: 'top',
                    labels: {
                        fontColor: '#333',
                        fontSize: 14,
                        fontStyle: 'normal',
                    },
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
            }
        });



		// Configuración del gráfico de ventas por producto
        var productSalesCtx = document.getElementById('productSalesChart').getContext('2d');
        var productSalesChart = new Chart(productSalesCtx, {
            type: 'bar',
            data: productSalesData, // Convierte la variable PHP a JSON
            options: {
                // Aquí puedes configurar más opciones del gráfico de ventas por producto
                responsive: true,
                maintainAspectRatio: false,
                title: {
                    display: true,
                    text: 'Ventas por Producto',
                    fontSize: 18,
                    fontColor: '#333',
                    fontStyle: 'bold',
                },
                scales: {
                    x: {
                        display: true,
                        title: {
                            display: true,
                            text: 'Producto',
                            fontSize: 14,
                            fontColor: '#666',
                            fontStyle: 'normal',
                        },
                    },
                    y: {
                        display: true,
                        title: {
                            display: true,
                            text: 'Total Ventas',
                            fontSize: 14,
                            fontColor: '#666',
                            fontStyle: 'normal',
                        },
                        ticks: {
                            beginAtZero: true,
                            stepSize: 100,
                        },
                    },
                },
                legend: {
                    display: false,
                },
            }
        });


		// Configuración del gráfico de distribución de ventas por categoría
		var categorySalesCtx = document.getElementById('categorySalesChart').getContext('2d');
		var categorySalesChart = new Chart(categorySalesCtx, {
        type: 'bar',
        
		data: {
            labels: categorySalesData['labels'], // Intercambiamos las etiquetas x y y
            datasets: [
                {
                    label: 'Distribución de Ventas por Categoría',
                    data: categorySalesData['datasets'][0]['data'], // Intercambiamos los datos x y y
                    backgroundColor: categorySalesData['datasets'][0]['backgroundColor'],
                }
            ]
        },
        options: {
            
            responsive: true,
            maintainAspectRatio: false,
            title: {
                display: true,
                text: 'Distribución de Ventas por Categoría (Barra Horizontal)',
                fontSize: 18,
                fontColor: '#333',
                fontStyle: 'bold',
            },
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    fontColor: '#666',
                },
            },
			plugins: {
                datalabels: { // Configuración de la extensión datalabels
                    anchor: 'end',
                    align: 'end',
                    display: function(context) {
                        return context.dataset.data[context.dataIndex] !== 0; // Mostrar etiquetas solo para valores distintos de cero
                    },
                    color: '#333',
                    font: {
                        weight: 'bold'
                    },
                    formatter: function(value) {
                        return '$' + value.toFixed(2); // Formato personalizado para mostrar el valor con dos decimales y el símbolo de dólar
                    }
                }
            },
            scales: {
                xAxes: [{
                    ticks: {
                        beginAtZero: true,
                    }
                }],
				yAxes: [{
                    ticks: {
                        stepSize: 200, // Ajusta el stepSize para limitar el número de ticks en el eje y
                    }
                }]
            }
        },
    });

	// Configuración del gráfico de marcas con más ventas
    var brandSalesCtx = document.getElementById('brandSalesChart').getContext('2d');
    var brandSalesChart = new Chart(brandSalesCtx, {
        type: 'bar',
        data: brandSalesData,
        options: {
            responsive: true,
            // maintainAspectRatio: false,
            title: {
                display: true,
                text: 'Marcas con Más Ventas',
                fontSize: 18,
                fontColor: '#333',
                fontStyle: 'bold',
            },
            legend: {
                display: false,
            },
            scales: {
                xAxes: [{
                    ticks: {
                        beginAtZero: true,
                    }
                }]
            }
        },
    });

    </script>


		</div>
	</section>
</div>
		<!-- /.content -->



@endsection



