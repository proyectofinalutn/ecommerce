@php
//obtener prefijo de la ruta actual
$prefix = Request::route()->getPrefix();
$route = Route::current()->getName();

@endphp


<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar-->
  <section class="sidebar">

    <div class="user-profile">
      <div class="ulogo">
        <a href="">
          <!-- logo for regular state and mobile devices -->
          <div class="d-flex align-items-center justify-content-center">
            <img src="{{ asset('backend/images/logoPS.png') }} " alt="">
            <h3><b>Picas Store</b></h3>
          </div>
        </a>
      </div>
    </div>

    <!-- sidebar menu-->
    <ul class="sidebar-menu" data-widget="tree">

      <li class="{{ ($route == 'dashboard')? 'active':'' }}">
        <a href="{{ url('admin/dashboard') }}">
          <i data-feather="pie-chart"></i>
          <span>Dashboard</span>
        </a>
      </li>

      @php
      $brand = (auth()->guard('admin')->user()->brand == 1);
      $category = (auth()->guard('admin')->user()->category == 1);
      $product = (auth()->guard('admin')->user()->product == 1);
      $slider = (auth()->guard('admin')->user()->slider == 1);
      $coupons = (auth()->guard('admin')->user()->coupons == 1);
      $shipping = (auth()->guard('admin')->user()->shipping == 1);
      $setting = (auth()->guard('admin')->user()->setting == 1);
      $returnorder = (auth()->guard('admin')->user()->returnorder == 1);
      $orders = (auth()->guard('admin')->user()->orders == 1);
      $stock = (auth()->guard('admin')->user()->stock == 1);
      $reports = (auth()->guard('admin')->user()->reports == 1);
      $alluser = (auth()->guard('admin')->user()->alluser == 1);
      $adminuserrole = (auth()->guard('admin')->user()->adminuserrole == 1);
      $setting = (auth()->guard('admin')->user()->setting == 1);

      @endphp

      @if($brand == true)
      <li class="treeview {{ ( $prefix == '/brand')?'active': '' }} ">
        <a href="#">
          <i data-feather="bookmark"></i> <span>Marcas</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="{{ ($route == 'all.brand')? 'active':'' }}"><a href="{{ route('all.brand')}}"><i class="ti-more"></i>Todas las marcas</a></li>
        </ul>
      </li>
      @else
      @endif

      @if($category == true)
      <li class="treeview {{ ( $prefix == '/category')?'active': '' }}">
        <a href="#">
          <i data-feather="layers"></i> <span>Categorías</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="{{ ($route == 'all.category')? 'active':'' }}"><a href="{{ route('all.category') }}"><i class="ti-more"></i>Todas las categorías</a></li>
          <li class="{{ ($route == 'all.subcategory')? 'active':'' }}"><a href="{{ route('all.subcategory') }}"><i class="ti-more"></i>Todas las subcategorías</a></li>
          <li class="{{ ($route == 'all.subsubcategory')? 'active':'' }}"><a href="{{ route('all.subsubcategory') }}"><i class="ti-more"></i>Todas las Sub-subcategorías</a></li>
        </ul>
      </li>
      @else
      @endif

      @if($product == true)
      <li class="treeview {{ ( $prefix == '/product')?'active': '' }}">
        <a href="#">
          <i data-feather="box"></i>
          <span>Productos</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="{{ ($route == 'add-product')? 'active':'' }}"><a href="{{ route('add-product') }}"><i class="ti-more"></i>Agregar Productos</a></li>
          <li class="{{ ($route == 'manage-product')? 'active':'' }}"><a href="{{ route('manage-product') }}"><i class="ti-more"></i>Administrar Productos</a></li>
        </ul>
      </li>
      @else
      @endif

      @if($slider == true)
      <li class="treeview {{ ( $prefix == '/slider')?'active': '' }}">
        <a href="#">
          <i data-feather="sliders"></i>
          <span>Slider</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="{{ ($route == 'manage-slider')? 'active':'' }}"><a href="{{ route('manage-slider') }}"><i class="ti-more"></i>Administrar Slider</a></li>
        </ul>
      </li>
      @else
      @endif


      @if($coupons == true)
      <li class="treeview {{ ( $prefix == '/coupons')?'active': '' }}">
        <a href="#">
          <i data-feather="tag"></i>
          <span>Cupones</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="{{ ($route == 'manage-coupon')? 'active':'' }}"><a href="{{ route('manage-coupon') }}"><i class="ti-more"></i>Administrar Cupones</a></li>
        </ul>
      </li>
      @else
      @endif

      @if($shipping == true)
      <li class="treeview {{ ( $prefix == '/shipping')?'active': '' }}">
        <a href="#">
          <i data-feather="truck"></i>
          <span>Zona de envíos</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="{{ ($route == 'manage-division')? 'active':'' }}"><a href="{{ route('manage-division') }}"><i class="ti-more"></i>Provincias</a></li>
        </ul>
        <ul class="treeview-menu">
          <li class="{{ ($route == 'manage-district')? 'active':'' }}"><a href="{{ route('manage-district') }}"><i class="ti-more"></i>Cantones</a></li>
        </ul>
        <ul class="treeview-menu">
          <li class="{{ ($route == 'manage-state')? 'active':'' }}"><a href="{{ route('manage-state') }}"><i class="ti-more"></i>Distritos</a></li>
        </ul>
      </li>
      @else
      @endif

      @if($returnorder == true)
      <li class="treeview {{ ($prefix == '/return')?'active':'' }}  ">
        <a href="#">
          <i data-feather="corner-up-left"></i>
          <span>Pedido de devolución</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="{{ ($route == 'return.request')? 'active':'' }}"><a href="{{ route('return.request') }}"><i class="ti-more"></i>Solicitud de devolución</a></li>

          <li class="{{ ($route == 'all.request')? 'active':'' }}"><a href="{{ route('all.request') }}"><i class="ti-more"></i>Todas las solicitudes</a></li>

        </ul>
      </li>

      @else
      @endif

      @if($setting == true)

      <li class="treeview {{ ($prefix == '/setting')?'active':'' }}  ">
        <a href="#">
          <i data-feather="settings"></i>
          <span>Administrador de configuración</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="{{ ($route == 'site.setting')? 'active':'' }}"><a href="{{ route('site.setting') }}"><i class="ti-more"></i>Configuración del sitio</a></li>
        </ul>
      </li>

      @else
      @endif

      <li class="header nav-small-cap">User Interface</li>

      @if($orders == true)
      <li class="treeview {{ ( $prefix == '/orders')?'active': '' }}">
        <a href="#">
          <i data-feather="file-plus"></i>
          <span>Pedidos</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="{{ ($route == 'pending-orders')? 'active':'' }}"><a href="{{ route('pending-orders') }}"><i class="ti-more"></i>Pedidos pendientes</a></li>

          <li class="{{ ($route == 'confirmed-orders')? 'active':'' }}"><a href="{{ route('confirmed-orders') }}"><i class="ti-more"></i>Pedidos confirmados</a></li>

          <li class="{{ ($route == 'processing-orders')? 'active':'' }}"><a href="{{ route('processing-orders') }}"><i class="ti-more"></i>Pedidos en proceso</a></li>

          <li class="{{ ($route == 'picked-orders')? 'active':'' }}"><a href="{{ route('picked-orders') }}"><i class="ti-more"></i> Pedidos seleccionados</a></li>

          <li class="{{ ($route == 'shipped-orders')? 'active':'' }}"><a href="{{ route('shipped-orders') }}"><i class="ti-more"></i> Pedidos enviados</a></li>

          <li class="{{ ($route == 'delivered-orders')? 'active':'' }}"><a href="{{ route('delivered-orders') }}"><i class="ti-more"></i> Pedidos entregados</a></li>

          <li class="{{ ($route == 'cancel-orders')? 'active':'' }}"><a href="{{ route('cancel-orders') }}"><i class="ti-more"></i> Pedidos cancelados</a></li>
        </ul>
      </li>
      @else
      @endif

      @if($stock == true)
      <li class="treeview {{ ($prefix == '/stock')?'active':'' }}  ">
        <a href="#">
          <i data-feather="box"></i>
          <span>Administrar existencias </span>
          <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="{{ ($route == 'product.stock')? 'active':'' }}"><a href="{{ route('product.stock') }}"><i class="ti-more"></i>Existencias de productos</a></li>


        </ul>
      </li>
      @else
      @endif

      @if($reports == true)
      <li class="treeview {{ ( $prefix == '/reports')?'active': '' }}">
        <a href="#">
          <i data-feather="bar-chart-2"></i>
          <span>Reportes</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="{{ ($route == 'all-reports')? 'active':'' }}"><a href="{{ route('all-reports') }}"><i class="ti-more"></i>Todos los reportes</a></li>

        </ul>
      </li>

      @else
      @endif

      @if($adminuserrole == true)
      <li class="treeview {{ ( $prefix == '/adminuserrole')?'active': '' }}">
        <a href="#">
          <i data-feather="bar-chart-2"></i>
          <span>Usuario Administrador</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="{{ ($route == 'all.admin.user')? 'active':'' }}"><a href="{{ route('all.admin.user') }}"><i class="ti-more"></i>Usuarios administradores</a></li>

        </ul>
      </li>
      @else
      @endif
    </ul>
  </section>

  <div class="sidebar-footer">
    <!-- item-->
    <a href="javascript:void(0)" class="link" data-toggle="tooltip" title="" data-original-title="Settings" aria-describedby="tooltip92529"><i class="ti-settings"></i></a>
    <!-- item-->
    <a href="mailbox_inbox.html" class="link" data-toggle="tooltip" title="" data-original-title="Email"><i class="ti-email"></i></a>
    <!-- item-->
    <a href="javascript:void(0)" class="link" data-toggle="tooltip" title="" data-original-title="Logout"><i class="ti-lock"></i></a>
  </div>
</aside>