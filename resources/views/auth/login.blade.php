@extends('frontend.main_master')
@section('content')

<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="{{ url('/') }}">Home</a></li>
				<li class='active'>Login</li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->

<div class="body-content">
	<div class="container">
		<div class="sign-in-page">
			<div class="row">
				<!-- Sign-in -->
				<div class="col-md-6 col-sm-6 sign-in">
					<h4 class="text-center">Iniciar sesión</h4>
					<p class="text-center">¡Hola! Bienvenido a tu cuenta.</p>

					<form method="POST" action="{{ isset($guard) ? url($guard.'/login') : route('login') }}">
						@csrf
						<div class="form-group">
							<label class="info-title" for="email">Correo electrónico <span>*</span></label>
							<input type="email" id="email" name="email" class="form-control unicase-form-control text-input">
							@error('email')
							<span class="invalid-feedback d-block" role="alert">
								<strong>{{ $message }}</strong>
							</span>
							@enderror
						</div>
						<div class="form-group">
							<label class="info-title" for="password">Contraseña <span>*</span></label>
							<input type="password" id="password" name="password" class="form-control unicase-form-control text-input">
							@error('password')
							<span class="invalid-feedback d-block" role="alert">
								<strong>{{ $message }}</strong>
							</span>
							@enderror
						</div>
						<div class="text-right">
							<a href="{{ route('password.request') }}" class="forgot-password">¿Olvidó su contraseña?</a>
						</div>
						<button type="submit" class="btn btn-primary btn-block mt-3"><i class="fa fa-sign-in"></i> Iniciar sesión</button>
						<button type="button" onclick="window.location.href='{{ route('register') }}'" class="btn btn-secondary btn-block mt-2">Registrarse</button>
						<!-- <a href="{{ route('register') }}" class="btn btn-secondary btn-block"><i class=""></i>Registrarse</a> -->

						<div class="social-sign-in outer-top-xs mt-3">
							<a href="{{ route('register') }}" class="btn btn-danger btn-block"><i class="fa fa-google"></i> Iniciar sesión con Google</a>
						</div>
					</form>
				</div>

				<!-- Sign-in -->

				<!-- create a new account -->
				<!-- <div class="col-md-6 col-sm-6 create-new-account" id="registerSection" style="display: none;">
					<h4 class="checkout-subtitle">Crear cuenta nueva</h4>
					<p class="text title-tag-line">Create your new account.</p>

					<form method="POST" action="{{ route('register') }}">
						@csrf
						<div class="form-group">
							<label class="info-title" for="exampleInputEmail2">Correo electrónico <span>*</span></label>
							<input type="email" id="email2" name="email" class="form-control unicase-form-control text-input">
							@error('email')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
							@enderror
						</div>
						<div class="form-group">
							<label class="info-title" for="exampleInputEmail1">Nombre <span>*</span></label>
							<input type="text" id="name" name="name" class="form-control unicase-form-control text-input">
							@error('name')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
							@enderror
						</div>
						<div class="form-group">
							<label class="info-title" for="exampleInputEmail1">Teléfono <span>*</span></label>
							<input type="text" id="phone" name="phone" class="form-control unicase-form-control text-input">
							@error('phone')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
							@enderror
						</div>
						<div class="form-group">
							<label class="info-title" for="exampleInputEmail1">Contraseña <span>*</span></label>
							<input type="password" id="password2" name="password" class="form-control unicase-form-control text-input">
							@error('password')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
							@enderror
						</div>
						<div class="form-group">
							<label class="info-title" for="exampleInputEmail1">Confirmar contraseña <span>*</span></label>
							<input type="password" id="password_confirmation" name="password_confirmation" class="form-control unicase-form-control text-input">
							@error('password_confirmation')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
							@enderror
						</div>
						<button type="submit" class="btn-upper btn btn-primary checkout-page-button">Sign Up</button>
					</form>


				</div> -->
				<!-- create a new account -->
			</div><!-- /.row -->
		</div><!-- /.sigin-in-->
		<!-- ============================================== BRANDS CAROUSEL ============================================== -->

		@include('frontend.body.brands')


		<!-- ============================================== BRANDS CAROUSEL : END ============================================== -->
	</div><!-- /.container -->
</div><!-- /.body-content -->


<script>
	document.addEventListener('DOMContentLoaded', function() {
		// const signInSection = document.getElementById('signInSection');
		const registerSection = document.getElementById('registerSection');
		const showRegisterButton = document.getElementById('showRegister');

		showRegisterButton.addEventListener('click', function() {
			// signInSection.style.display = 'none';
			registerSection.style.display = 'block';
		});
	});
</script>

@endsection