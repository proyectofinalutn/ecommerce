@extends('frontend.main_master')
@section('content')

<div class="body-content">
    <div class="container">
        <div class="row">
            @include('frontend.common.user_sidebar')


            <div class="col-md-2">

            </div>
            <div class="col-md-6">
                <div class="card">
                    <h3 class="text-center"><span class="text-danger"></span><strong></strong> Actualizar Perfil </h3>
                    <div class="card-body">
                        <form method="post" action="{{ route('user.profile.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label class="info-title" for="exampleInputEmail1">Nombre <span>*</span></label>
                            </div>
                            <input type="text" id="name" name="name" class="form-control unicase-form-control text-input" value="{{ $user->name }}">
                            <div class="form-group">
                                <label class="info-title" for="exampleInputEmail1">Correo <span>*</span></label>
                                <input type="email" id="email" name="email" class="form-control unicase-form-control text-input" value="{{ $user->email }}">
                            </div>
                            <div class="form-group">
                                <label class="info-title" for="exampleInputEmail1">Teléfono <span>*</span></label>
                                <input type="text" id="phone" name="phone" class="form-control unicase-form-control text-input" value="{{ $user->phone }}">
                            </div>
                            <div class="form-group">
                                <label class="info-title" for="exampleInputEmail1">Foto de perfil <span></span></label>
                                <input type="file" id="file" name="profile_photo_path" class="form-control unicase-form-control text-input">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Actualizar</button>
                                <button href="{{ route('dashboard') }}" class="btn btn-danger">Cancelar</button>
                            </div>

                        </form>

                    </div>

                </div>

            </div>

        </div>

    </div>


</div>

@endsection