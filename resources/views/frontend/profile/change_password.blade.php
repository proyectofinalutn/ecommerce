@extends('frontend.main_master')
@section('content')

<div class="body-content">
    <div class="container">
        <div class="row">

        @include('frontend.common.user_sidebar')

            <div class="col-md-2">
            </div>
            <div class="col-md-6">
                <div class="card">
                    <h3 class="text-center"><span class="text-danger">Cambiar Contraseña </span><strong></strong></h3>
                    <div class="card-body">
                        <form method="post" action="{{ route('user.password.update') }}">
                            @csrf
                            <div class="form-group">
                                <label class="info-title" for="exampleInputEmail1">Contraseña Actual <span>*</span></label>
                                <input type="password" id="current_password" name="oldpassword" class="form-control" >
                                @error('oldpassword')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label class="info-title" for="exampleInputEmail1">Contraseña Nueva <span>*</span></label>
                                <input type="password" id="password" name="password" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label class="info-title" for="exampleInputEmail1">Confirmar Contraseña <span>*</span></label>
                                <input type="password" id="password_confirmation" name="password_confirmation" class="form-control" >
                                @error('password')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Actualizar</button>
                                <button href="{{ route('dashboard') }}" class="btn btn-danger">Cancelar</button>
                            </div>

                        </form>

                    </div>

                </div>

            </div>

        </div>

    </div>


</div>

@endsection