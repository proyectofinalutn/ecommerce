@php
$id = Auth::user()->id;
$user = App\Models\User::find($id);

//obtener prefijo de la ruta actual
$prefix = Request::route()->getPrefix();
$route = Route::current()->getName();


@endphp


<div class="col-md-2"><br>
    <img class="card-img-top" style="border-radius: 50%" src="{{ (!empty($user->profile_photo_path))? url('upload/user_images/'.$user->profile_photo_path):url('upload/no_image.jpg') }}" height="100%" width="100%"><br><br>

    <ul class="list-group list-group-flush">
        <a href="{{ ($route == 'dashboard')? 'active':'' }}" class="btn btn-primary btn-sm btn-block">Home</a>

        <a href="{{ route('user.profile') }}" class="btn btn-primary btn-sm btn-block {{ ($route == 'user.profile') ? 'active' : '' }}">Actualizar perfil</a>

        <a href="{{ route('change.password') }}" class="btn btn-primary btn-sm btn-block {{ ($route == 'change.password') ? 'active' : '' }}">Cambiar contraseña </a>
        
        <a href="{{ route('my.orders') }}" class="btn btn-primary btn-sm btn-block {{ ($route == 'my.orders') ? 'active' : '' }}">Mis pedidos</a>
        
        <a href="{{ route('return.order.list') }}" class="btn btn-primary btn-sm btn-block {{ ($route == 'return.order.list') ? 'active' : '' }}">Devoluciones de pedidos</a>
        
        <a href="{{ route('cancel.orders') }}" class="btn btn-primary btn-sm btn-block {{ ($route == 'cancel.orders') ? 'active' : '' }}">Pedidos cancelados</a>

        <a href="{{ route('user.logout') }}" class="btn btn-danger btn-sm btn-block ">Cerrar sesión</a>

    </ul>

</div> <!-- // end col md 2 -->