@extends('frontend.main_master')
@section('content')

<div class="body-content">
    <div class="container">
        <div class="row">
            @include('frontend.common.user_sidebar')

            <div class="col-md-5">
                <div class="card">
                    <div class="card-header">
                        <h4>Detalles del envío</h4>
                    </div>
                    <hr>
                    <div class="card-body" style="background: #E9EBEC;">
                        <table class="table">
                            <tr>
                                <th> Nombre del comprador: </th>
                                <th> {{ $order->name }} </th>
                            </tr>

                            <tr>
                                <th> Teléfono del comprador: </th>
                                <th> {{ $order->phone }} </th>
                            </tr>

                            <tr>
                                <th> Correo del comprador : </th>
                                <th> {{ $order->email }} </th>
                            </tr>

                            <tr>
                                <th> Cantón : </th>
                                <th> {{ $order->division->division_name }} </th>
                            </tr>

                            <tr>
                                <th> Distrito : </th>
                                <th> {{ $order->district->district_name }} </th>
                            </tr>

                            <tr>
                                <th> Provincia : </th>
                                <th>{{ $order->state->state_name }} </th>
                            </tr>

                            <tr>
                                <th> Código postal : </th>
                                <th> {{ $order->post_code }} </th>
                            </tr>

                            <tr>
                                <th> Fecha de la compra : </th>
                                <th> {{ $order->order_date }} </th>
                            </tr>

                        </table>

                    </div>

                </div>

            </div> <!-- // end col md -5 -->

            <div class="col-md-5">
                <div class="card">
                    <div class="card-header">
                        <h4>Detalles del pedido
                            <span class="text-danger"> Factura : {{ $order->invoice_no }}</span>
                        </h4>
                    </div>
                    <hr>
                    <div class="card-body" style="background: #E9EBEC;">
                        <table class="table">
                            <tr>
                                <th> Nombre : </th>
                                <th> {{ $order->user->name }} </th>
                            </tr>

                            <tr>
                                <th> Teléfono : </th>
                                <th> {{ $order->user->phone }} </th>
                            </tr>

                            <tr>
                                <th> Tipo de pago : </th>
                                <th> {{ $order->payment_method }} </th>
                            </tr>

                            <tr>
                                <th> ID transacción : </th>
                                <th> {{ $order->transaction_id }} </th>
                            </tr>

                            <tr>
                                <th> Factura : </th>
                                <th class="text-danger"> {{ $order->invoice_no }} </th>
                            </tr>

                            <tr>
                                <th> Monto a pagar: </th>
                                <th>₡{{ $order->amount }} </th>
                            </tr>

                            <tr>
                                <th> Estado pedido : </th>
                                <th>
                                    <span class="badge badge-pill badge-warning" style="background: #418DB9;">{{ $order->status }} </span>
                                </th>
                            </tr>

                        </table>


                    </div>

                </div>

            </div> <!-- // 2ND end col md -5 -->

            <div class="row">

                <div class="col-md-12">

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>

                                <tr style="background: #e2e2e2;">
                                    <td class="col-md-1">
                                        <label for=""> Imagen</label>
                                    </td>

                                    <td class="col-md-3">
                                        <label for=""> Nombre del producto </label>
                                    </td>

                                    <td class="col-md-3">
                                        <label for=""> Código del producto</label>
                                    </td>


                                    <td class="col-md-2">
                                        <label for=""> Color </label>
                                    </td>

                                    <td class="col-md-2">
                                        <label for=""> Tamaño </label>
                                    </td>

                                    <td class="col-md-1">
                                        <label for=""> Cantidad </label>
                                    </td>

                                    <td class="col-md-1">
                                        <label for=""> Precio </label>
                                    </td>
                                    <td class="col-md-1">
                                        <label for=""> Download </label>
                                    </td>

                                </tr>

                                @foreach($orderItem as $item)
                                <tr>
                                    <td class="col-md-1">
                                        <label for=""><img src="{{ asset($item->product->product_thambnail) }}" height="50px;" width="50px;"> </label>
                                    </td>

                                    <td class="col-md-3">
                                        <label for=""> {{ $item->product->product_name }}</label>
                                    </td>


                                    <td class="col-md-3">
                                        <label for=""> {{ $item->product->product_code }}</label>
                                    </td>

                                    <td class="col-md-2">
                                        <label for=""> {{ $item->color }}</label>
                                    </td>

                                    <td class="col-md-2">
                                        <label for=""> {{ $item->size }}</label>
                                    </td>

                                    <td class="col-md-2">
                                        <label for=""> {{ $item->qty }}</label>
                                    </td>

                                    <td class="col-md-2">
                                        <label for=""> ₡{{ $item->price }} ( ₡ {{ $item->price * $item->qty}} ) </label>
                                    </td>
                                    
                                    @php

                                    $file = App\Models\Product::where('id',$item->product_id)->first();
                                    @endphp

                                    <td class="col-md-1">
                                        @if($order->status == 'Pendiente')
                                        <strong>
                                        <span class="badge badge-pill badge-success" style="background: #418DB9;"> No File</span>
                                        </strong>

                                        @elseif($order->status == 'confirmado')

                                        <a target="_blank" href="{{ asset('upload/pdf/'.$file->digital_file) }}">
                                        <strong>
                                            <span class="badge badge-pill badge-success" style="background: #FF0000;"> Download Ready</span>
                                        </strong> </a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>

                        </table>

                    </div>


                </div> <!-- / end col md 8 -->


            </div> <!-- // END ORDER ITEM ROW -->
            @if($order->status !== "entregado")

            @else

            @php
            $order = App\Models\Order::where('id',$order->id)->where('return_reason','=',NULL)->first();
            @endphp

            @if($order)
            <form action="{{ route('return.order',$order->id) }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="label"> Motivo de devolución del pedido:</label>
                    <textarea name="return_reason" id="" class="form-control" cols="30" rows="05"></textarea>
                </div>
                <button type="submit" class="btn btn-danger">Solicitar devolución de pedido</button>
            </form>
            @else

            <span class="badge badge-pill badge-warning" style="background: red">Ha enviado una solicitud de devolución para este producto</span>
            @endif 


            @endif
            <br><br>

        </div> <!-- // end row -->

    </div>

</div>


@endsection